 console.log("Hello from script");

 function pageLoaded(){
 
 // get a handle to the canvas context
    const canvas = document.getElementById("the_canvas");

    // get 2D context for this canvas
    const context = canvas.getContext("2d");

    // draw something
    context.fillStyle = "blue";

    context.fillRect(0, 0, 70, 70);
    //fillRect(position x, position y, width, height)

    //creating second square
    context.fillStyle = "blue";
    //positioning second square
    context.fillRect(730, 0, 70, 70);

    //creating third square
    context.fillStyle = "blue";
    //positioning third square
    context.fillRect(0, 730, 70, 70);

    //creating fourth square
    context.fillStyle = "blue";
    //positioning fourth square
    context.fillRect(730, 730, 70, 70);

    //creating hexagon 
    context.beginPath();
    context.moveTo(100, 200);//starting point
    //drawing sides
    context.lineTo(150, 150);
    context.lineTo(250, 150);
    context.lineTo(300, 200);
    context.lineTo(250, 250);
    context.lineTo(150, 250);
    context.lineTo(100, 200);

    context.closePath(); //close the path

    //fill the hexagon with color
    context.fillStyle = "pink";
    context.fill();

    //octogon
    context.beginPath();
    context.moveTo(400, 400);

    //draw 8 sides for octogon
    context.lineTo(500, 400);
    context.lineTo(550, 450);
    context.lineTo(550, 550);
    context.lineTo(500, 600);
    context.lineTo(400, 600);
    context.lineTo(350, 550);
    context.lineTo(350, 450);
    context.lineTo(400, 400);

    context.closePath(); //closing path

    context.fillStyle = "orange"; // filling color
    context.fill();

    //creating a star
    context.beginPath();
    context.moveTo(500, 500);

    //creating a star at the starting point (600, 600)
    context.beginPath();
    context.moveTo(500, 500); //starting position

    //drawing the sides of the star
    context.lineTo(550, 600);
    context.lineTo(650, 600);
    context.lineTo(575, 650);
    context.lineTo(650, 700);
    context.lineTo(550, 700);
    context.lineTo(500, 800);
    context.lineTo(450, 700);
    context.lineTo(350, 700);
    context.lineTo(425, 650);
    context.lineTo(350, 600);
    context.lineTo(450, 600);
    context.lineTo(500, 500);

    context.closePath(); //closing path

    //color star
    context.fillStyle = "gold";
    context.fill();

    //stroking outline
    context.lineWidth = 5; //thickness of outline
    context.strokeStyle = "black";
    context.stroke();

    //creating heart
    context.beginPath();
    context.moveTo(200, 300);// move to staring position
 
    //drawing left curve of heart
    context.bezierCurveTo(200, 250, 150, 200 , 100, 200);

    //drawing the point of the heart
    context.bezierCurveTo(50, 200, 50, 250, 100, 300);

    //drawing the right curve of heart
    context.bezierCurveTo(150, 350, 200, 400, 200, 450);

    context.closePath();

    //fill the heart shape 
    context.fillStyle = "red";
    context.fill();

    //drawing a cross
    context.beginPath();
    context.moveTo(50, 50);

    //draw the arrow body
    context.lineTo(150, 50);
    context.lineTo(150, 30);
    context.lineTo(200, 75);
    context.lineTo(150, 120);
    context.lineTo(150, 100);
    context.lineTo(50, 100);

    //draw arrow head 
    context.lineTo(50, 80);
    context.lineTo(0, 80);
    context.lineTo(0, 70);
    context.lineTo(50, 50);

    context.closePath();

    //fill arrow 
    context.fillStyle = "green";
    context.fill();

    //setting up text element
    context.font = "20px Arial";
    context.fillStyle = "black";

    //creating a text element saying arial
    context.fillText("Arial", 80, 80);

    //setting up text element
    context.font = "20px Lucida Console";
    context.fillStyle = "black";

    //creating a text element saying Lucida Console
    context.fillText("Lucida Console", 100, 100);

    //setting up text element
    context.font = "30px Baskerville";
    context.fillStyle = "black";

    //creating a text element saying Baskerville
    context.fillText("Baskerville", 200, 200);
 }

 pageLoaded();
    //var key word telling javascript you're declaring a variable
    //let use to declare a variabled that is block scaped in javascript
    //const use to declare a const (unchangeable) variable

    
  
    






    
